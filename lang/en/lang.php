<?php return [
    'plugin' => [
        'name' => 'Theme Link',
        'description' => 'Adds a back-end menu link to the theme configuration for the active theme.'
    ],
    'menu' => [
        'label' => 'Theme Options'
    ],
    'settings' => [
        'label' => 'Theme Link Settings',
        'description' => 'Manage theme link settings.',
        'order_label' => 'Order of Theme Link',
        'order_comment' => 'Enter a number to determine the position of the theme link in the menu. Defaults to 950.'
    ]
];