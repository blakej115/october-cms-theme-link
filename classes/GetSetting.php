<?php namespace BlakeJones\ThemeLink\Classes;

use BlakeJones\ThemeLink\Models\ThemeLinkSetting;

class GetSetting {
    public static function get($setting, $default) {
        $settingVal = ThemeLinkSetting::get($setting, $default);
        // Use default when a setting field is blank.
        if (empty($settingVal)) {
            $settingVal = $default;
        }
        return $settingVal;
    }
}