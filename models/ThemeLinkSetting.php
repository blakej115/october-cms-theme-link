<?php namespace BlakeJones\ThemeLink\Models;

use Model;

class ThemeLinkSetting extends Model
{
    public $implement = [
        \System\Behaviors\SettingsModel::class
    ];

    public $settingsCode = 'blakejones_themelink_settings';

    public $settingsFields = 'fields.yaml';
}