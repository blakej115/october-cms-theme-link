<?php namespace BlakeJones\ThemeLink;

use Backend;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use BlakeJones\ThemeLink\Models\ThemeLinkSetting;
use BlakeJones\ThemeLink\Classes\GetSetting;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'blakejones.themelink::lang.plugin.name',
            'description' => 'blakejones.themelink::lang.plugin.description',
            'author' => 'Blake Jones',
            'iconSvg' => 'plugins/blakejones/themelink/assets/images/icon.png',
            'homepage' => 'https://blakejones.dev/'
        ];
    }

    public function registerNavigation()
    {
        return [
            'theme' => [
                'label' => 'blakejones.themelink::lang.menu.label',
                'url' => Backend::url('cms/themeoptions/update'),
                'iconSvg' => 'plugins/blakejones/themelink/assets/images/icon.png',
                'permissions' => ['cms.theme_customize'],
                'order' => GetSetting::get('order', 950)
            ]
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'blakejones.themelink::lang.settings.label',
                'description' => 'blakejones.themelink::lang.settings.description',
                'category' => SettingsManager::CATEGORY_CMS,
                'icon' => 'icon-paint-brush',
                'class' => ThemeLinkSetting::class,
                'permissions' => ['cms.themes']
            ]
        ];
    }
}
