# Theme Link

Extends the top back-end menu with a link to the CMS theme configuration page for the active theme.

No configuration is required, but there is a setting to set the "order" where the Theme Options link appears.

The theme link appearing at the top is dependent upon the "Customize Theme" permission and the ability to edit the order is dependent upon the "Manage Themes" permission.